var ContactUs = function () {

    return {
        //main function to initiate the module
        init: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
	            lat: 10.6812179,
				lng: -74.990195,
				zoom: 10
			  });
			   var marker = map.addMarker({
		            lat: 11.001302,
					lng: -74.795843,
		            title: 'La Casona Del Prado',
		            infoWindow: {
		                content: "<b>La Casona Del Prado</b><br> Calle 70 # 60-11"
		            }
		        });

			   var markerTwo = map.addMarker({
			   		lat: 11.0006082,
					lng: -74.8029951,
		            title: 'Centro de Capacitación',
		            infoWindow: {
		                content: "<b>Centro de Capacitación y Desarrollo Empresarial</b><br> Carrera 54 No. 74"
		            }
			   });

			   var markerThree = map.addMarker({
			   		lat: 10.9917939,
					lng: -74.7913005,
		            title: 'Jardín Infantil',
		            infoWindow: {
		                content: "<b>Jardín Infantil</b><br> Cra. 53 No. 55 - 210"
		            }
			   });

			   var markerFour = map.addMarker({
			   		lat: 10.9889245,
					lng: -74.789819,
		            title: 'Oficinas de Atención al Cliente',
		            infoWindow: {
		                content: "<b>Oficinas de Atención al Cliente</b><br> Cra 46 No 53 - 34 Piso 1<br><br> <b>Oficinas Administrativas</b><br> Piso 2"
		            }
			   });

			   var markerFive = map.addMarker({
			   		lat: 10.9593727,
					lng: -74.768591,
		            title: 'Centro de Atención Infantil La Luz',
		            infoWindow: {
		                content: "<b>Centro de Atención Infantil La Luz</b><br> Cra 21 No. 6 - 18"
		            }
			   });

			   var markerSix = map.addMarker({
			   		lat: 10.9169009,
					lng: -74.7653619,
		            title: 'Soledad',
		            infoWindow: {
		                content: "<b>Soledad</b><br> Calle 18 No 19 – 28 Lc 10 P2"
		            }
			   });

			   var markerSeven = map.addMarker({
			   		lat: 10.6299469,
					lng: -74.9208858,
		            title: 'Sabanalarga',
		            infoWindow: {
		                content: "<b>Sabanalarga</b><br> Calle 21 No 17 – 39 Lc 1 "
		            }
			   });

			   var markerEigth = map.addMarker({
			   		lat: 10.8030931,
					lng: -75.1803875,
		            title: 'Centro Recreacional El Descanso',
		            infoWindow: {
		                content: "<b>Centro Recreacional El Descanso</b><br> Via al Mar, Entrada a Santa Verónica"
		            }
			   });

			   marker.infoWindow.open(map, marker, markerTwo, markerThree, markerFour);
			});
        }
    };

}();